﻿// See https://aka.ms/new-console-template for more information

using CIExample.Core;
using Microsoft.Extensions.DependencyInjection;

public class Program
{
    public static void Main(string[] args)
    {
        //setup our DI
        var serviceProvider = new ServiceCollection()
            .AddSingleton<IMagicMath, MagicMath>()
            .BuildServiceProvider();
        
        //do the actual work here
        var magic = serviceProvider.GetService<IMagicMath>();

        Console.WriteLine(magic.AddMagicSeven(2));
        var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
        Console.WriteLine($"Environment variable: {env}");
        
        if (env == "CI_CHECK")
        {
            Console.WriteLine("CI was successful.");
        }
        else
        {
            Console.WriteLine($"The program is working fine. Env: {env}");
        }
    }
}