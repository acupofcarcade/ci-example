﻿namespace CIExample.Core;

public class MagicMath : IMagicMath
{
    private const int MagicSeven = 7;
    
    public int AddMagicSeven(int number)
    {
        return number + MagicSeven;
    }
}