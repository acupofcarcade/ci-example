using Xunit;

namespace CIExample.Core.Tests;

public class MagicMathTests
{
    [Theory]
    [InlineData(2, 9)]
    [InlineData(3, 10)]
    [InlineData(4, 11)]
    [InlineData(5, 12)]
    public void AddMagicSeven_Number_AddedSeven(int number, int result)
    {
        // Arrange
        var magic = new MagicMath();

        // Act
        var magicResult = magic.AddMagicSeven(number);
        
        // Assert
        Assert.Equal(result, magicResult);
    }
}